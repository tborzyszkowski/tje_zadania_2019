### Treść
Bazując na przykładzie 
[``springmvcdemo``](https://gitlab.com/technologie-java-enterprise-2019/lectures/tree/master/springmvcdemo) 
z wykładu stworzyć projekt 
o następującej funkcjonalności:
1. Na własnym przykładzie 3-4 klas zamodelować własną *dziedzinę danych* aplikacji wykorzystując:
    ```@Entity```, ```@Id```, ```@GeneratedValue```. 
    Powinna być ona tak dobrana,
    by wykorzystać wszystkie rodzaje połączeń pomiędzy encjami 
    (```@OneToOne```, ```@OneToMany```, ```@ManyToOne```, ```@ManyToMany```).
2. Przygotować wzorując się na aplikacji ``springmvcdemo`` z wykładu własną aplikację 
    webową oraz REST API, umożliwiającą realizację CRUD na danych zdefiniowanych w punkcie 1.
3. Dla opracowanego REST API przygotować testy w Postmanie i dołączyć ich definicje do projektu.

### Wejście
Szkic projektu ```springmvcdemo``` z wykładu.
### Wyjście
Działająca aplikacja webowa oraz REST API, wg specyfikacji przedstawionej w treści zadania.

### Warunki poprawności zadania
1. Realizacja:
    - mapowania klas oraz relacji między nimi, jak opisano w treści zadania
    - aplikacji web i REST API realizujących CRUD na zdefiniowanej dziedzinie
    - testy w Postmanie
2. Student umie zaprezentować i wyjaśnić działanie rozwiazania zadania.

### Termin oddania: 10.01.2020
Przekroczenie terminu o *n* zajęć wiąże się z karą:
- punkty uzyskania za realizację zadania są dzielone przez *2<sup>n</sup>*.

### Punkty za zadanie: 15
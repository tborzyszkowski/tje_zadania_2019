### Treść

1. Opracować przykłady ilustrujące przetwarzanie strumiemi w ```Spring Reactor```, 
    ze szczególnym uwzględnieniem wykorzystania typów ```Mono``` oraz ```Flux```.
    Zaprezentować jakie są główne różnice w przetwarzaniu sekwencji danych przez ```Spring Reactor```
    w porównaniu z ```Java Streams```.
    
    Źródło: [Project Reactor](https://projectreactor.io/learn)
    
2. Przygotować aplikację web oraz REST API bazującą na ```WebFlux```.
   Aplikacja powinna być oparta na dwóch tabelach połączonych relacją jeden-wiele. 
   Można oprzeć się na danych  *in memory*.
   
   Źródło: [Web on Reactive Stack](https://docs.spring.io/spring/docs/current/spring-framework-reference/web-reactive.html)
   
3. Używając odpowiednich narzędzi diagnostycznych pokazać przewagi aplikacji bazującej na ```WebFlux```
    w porównaiu ze zwykłą aplikacją Web.

### Wejście
Wykład z *Technologi Java Enterprise* oraz powyższe źródła.
### Wyjście
Działająca aplikacja webowa oraz REST API oparta an ```WebFlux``` , 
wg specyfikacji przedstawionej w treści zadania.

### Warunki poprawności zadania
1. Realizacja:
    - przykłady do p. 1.
    - aplikacja Web i REST API realizujących CRUD na zdefiniowanej dziedzinie
2. Student umie zaprezentować i wyjaśnić działanie rozwiazania zadania.

### Termin oddania: 24.01.2020
Przekroczenie terminu o *n* zajęć wiąże się z karą:
- punkty uzyskania za realizację zadania są dzielone przez *2<sup>n</sup>*.

### Punkty za zadanie: 15
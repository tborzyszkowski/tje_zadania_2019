package com.example.handlingformsubmission;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class PersonsController {

    private List<Person> persons = new ArrayList<>();

    @GetMapping("/persons")
    public String greetingForm(Model model) {

        model.addAttribute("personsattr", new Person());
        return "personsattr";
    }

    @PostMapping("/addperson")
    public String greetingSubmit(@ModelAttribute Person person) {
        persons.add(person);
        return "result";
    }

}

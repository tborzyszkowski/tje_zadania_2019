### Treść
Dla wybranej hierarchii klas (rodzina, domeny, komputery, ... ). 
Zaprezentować działanie adnotacji: `@Component`, `@Autowired`, `@Configuration`, `@Bean`
do konfiguracji zależności w Spring.

### Wejście
Projekt wygenerowany za pomocą Spring Initializr 

### Wyjście
Działająca aplikacja webowa.

### Warunki poprawności zadania
1. Student umie wyjaśnić na skonstruowanym przykładzie jak na zadanej hierarchii klas działają adnotacje: `@Component`, `@Autowired`, `@Configuration`, `@Bean`. 
2. Omawiane obiekty mogą generować tekst na standardowe wyjście.
3. Student powinien zaprezentować jak dla adnotacji `@Autowired` działa dopasowanie przez zgodność:
	- typów
	- kwalifikatorów 
	- nazw.

### Termin oddania: 25.10.2019
Przekroczenie terminu o *n* zajęć wiąże się z karą:
- punkty uzyskania za realizację zadania są dzielone przez *2<sup>n</sup>*.

### Punkty za zadanie: 10